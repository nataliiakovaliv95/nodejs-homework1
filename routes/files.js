const express = require('express');
const router = express.Router();
const filesService = require('../services/filesService');
const validationService = require('../services/validationService');

router.post('/', async (req, res) => {
  const { filename, content } = req.body;
  const method = req.method;
  const err = validationService.reqBodyValidation(filename, content, method);
  if (err) {
    res.status(400).json({
      message: err
    });
  } else {
    try {
      await filesService.saveFile(filename, content);
      res.status(200).json({
        message: 'File created successfully'
      });
    } catch (err) {
      res.status(500).json({
        message: 'Server error'
      });
      console.log(err)
    }
  }
});

router.get('/', async (req, res) => {
  try {
    const files = await filesService.getFiles();
    res.status(200).json({
      message: 'Success',
      files
    });
  } catch (err) {
    res.status(500).json({
      message: 'Server error'
    });
    console.log(err)
  }
});

router.get('/:filename', async (req, res) => {
  const filename = req.params.filename;
  try {
    const status = await filesService.getFileStatus(filename);
    try {
      const fileData = await filesService.getFile(filename);
      res.status(200).json({
        message: 'Success',
        filename,
        content: fileData,
        extension: filesService.getFileExtension(filename),
        uploadedDate: status.birthtime,
      });
    } catch (err) {
      res.status(500).json({
        message: 'Server error'
      });
      console.log(err)
    }
  } catch (err) {
    res.status(400).json({
      message: `No file with '${filename}' filename found`
    });
    console.log(err)
  }
});

router.put('/', async (req, res) => {
  const { filename, content } = req.body;
  const method = req.method;
  const err = validationService.reqBodyValidation(filename, content, method);
  if (err) {
    res.status(400).json({
      message: err
    });
  } else {
    try {
      await filesService.saveFile(filename, content);
      res.status(200).json({
        message: 'File updated successfully'
      });
    } catch (err) {
      res.status(500).json({
        message: 'Server error'
      });
      console.log(err)
    }
  }
});

router.delete('/:filename', async (req, res) => {
  const filename = req.params.filename;
  try {
    await filesService.deleteFile(filename);
    res.status(200).json({
      message: 'File deleted successfully'
    });
  } catch (err) {
    res.status(400).json({
      message: `No file with '${filename}' filename found`
    });
    console.log(err)
  }
})

module.exports = router;