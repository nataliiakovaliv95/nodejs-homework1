const fs = require('fs');
const path = require('path');
const fsPromises = fs.promises;

const filesFolder = path.join(__dirname, '..', 'files');

try {
  if (!fs.existsSync(filesFolder)) {
    fs.mkdirSync(filesFolder);
  }
} catch (err) {
  throw err
}


const getFilePath = filename => path.join(filesFolder, filename);

const getFileExtension = filename => filename.match(/(log|txt|json|yaml|xml|js)$/g)[0];

const isCorrectFileExtension = filename => filename.match(/^.+\.(log|txt|json|yaml|xml|js)$/g);

const isFileExist = filename => fs.existsSync(getFilePath(filename));

const getFileStatus = filename => fsPromises.stat(getFilePath(filename));

const saveFile = (filename, content) => fsPromises.writeFile(getFilePath(filename), content);

const getFiles = () => fsPromises.readdir(filesFolder);

const getFile = filename => fsPromises.readFile(getFilePath(filename), "utf8");

const deleteFile = filename => fsPromises.unlink(getFilePath(filename));

module.exports = {
  getFileStatus,
  getFileExtension,
  saveFile,
  getFiles,
  getFile,
  deleteFile,
  isFileExist,
  isCorrectFileExtension
};
