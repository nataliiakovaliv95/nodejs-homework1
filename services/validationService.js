const filesService = require('./filesService');

const reqBodyValidation = (filename, content, method) => {
  if (!filename) {
    return "Please specify 'filename' parameter"
  } else if (!content) {
    return "Please specify 'content' parameter"
  } else if (!filesService.isCorrectFileExtension(filename)) {
    return 'File extension is not correct'
  } else {
    const fileExist = filesService.isFileExist(filename);
    if (fileExist && method === 'POST') {
      return `File '${filename}' already exists`
    } else if (!fileExist && method === 'PUT') {
      return `No file with '${filename}' filename found`
    }
  }
}

module.exports = {
  reqBodyValidation
};